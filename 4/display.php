<!-- Skript: display.php  Author: Voicescu Adrian  Date: 20.09.2021  Version: 1.0 -->

<?php

    $values  = array();
    // Create connection
    $conn = new mysqli("localhost", "root", "");
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "select email, name, phone, married, sex from test.user where email = '".htmlspecialchars($_GET["email"])."';";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $values['email'] = $row["email"];
            $values['name'] = $row["name"];
            $values['phone'] = $row["phone"] ? $row["phone"] : "No phone number";
            $values['married'] = $row["married"] ? "Yes" : "No";
            $values['sex'] = $row["sex"];
        }
      } else {
        echo "0 results";
      }

    mysqli_close($conn);  
    
?>

<!doctype html>
<html>
<body>
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">

 <p style="color: red;"><b>Hello, welcome to the display page <?php echo htmlspecialchars(@$values['name']) ?> : </b><br />
 
 </p>

 <p>Name: <?php echo htmlspecialchars(@$values['name']) ?></P>
 <P>Email: <?php echo htmlspecialchars(@$values['email']) ?></p>
 <P>Phone: <?php echo htmlspecialchars(@$values['phone']) ?></p>
 <P>Married: <?php echo htmlspecialchars(@$values['married']) ?></p>
 <p>Sex: <?php echo htmlspecialchars(@$values['sex']) ?></p>

</form>
</body>
</html>