<!-- Skript: index.php  Author: Voicescu Adrian  Date: 20.09.2021  Version: 1.0 -->

<?php
$myemail = "comments@myemail.com";
$errors  = array();
$values  = array();
$errmsg  = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(formular_validieren()){
        formular_ausgeben();
    }
}

function formular_ausgeben(){
    $conn = new mysqli("localhost", "root", "");
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
        echo "Failed to connect to the database <br />";
    }
    else{
        global $values, $errmsg;
        $stmt = $conn->prepare("INSERT INTO `test`.`user` (`email`, `name`, `phone`, `married`, `sex`) VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param('sssis', $values['email'], $values['yourname'], $values['phone'], $values['married'], $values['sex']);
        $stmt->execute();
        
    }

    if($conn->error){
        $errmsg = $conn->error;
    }
    else{
        echo "Form was OK!  Good to process...<br />";
        header("Location: http://localhost/new/display.php?email=".$values['email']);
        $values = array(); // empty values array
    }

    mysqli_close($conn);  
    
}


function formular_validieren(){
    global $myemail, $errors, $values, $errmsg;

    foreach($_POST as $key => $value) {
        $values[$key] = htmlspecialchars(trim(stripslashes($value))); // basic input filter against spaces and html special characters
    }

    if (check_input($values['yourname']) == false) { 
        $errors['yourname'] = 'Enter your name!';
    }

    if (check_input($values['email']) == false) {
        $errors['email'] = 'Please enter your email address.';
    } else if (!preg_match('/([\w\-]+\@[\w\-]+\.[\w\-]+)/', $values['email'])) {
        $errors['email'] = 'Invalid email address format.';
    }

    if (sizeof($errors) == 0) {
        // you can process your for here and redirect or show a success message
        return true;
    } else {
        // one or more errors
        foreach($errors as $error) {
            $errmsg .= $error . '<br />';
        }
        return false;
    }
}

function check_input($input) {
    if (!isset($input) || strlen($input) == 0) {
        return false;
    } else {
        // TODO: in case of other checks
        return true;
    }
}
?>
<!doctype html>
<html>
<body>
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
 <?php if ($errmsg != ''): ?>
 <p style="color: red;"><b>Please correct the following errors:</b><br />
 <?php echo $errmsg; ?>
 </p>
 <?php endif; ?>

 <p>Name: <input type="text" name="yourname" value="<?php echo htmlspecialchars(@$values['yourname']) ?>" /></P>
 <P>Email: <input type="text" name="email" value="<?php echo htmlspecialchars(@$values['email']) ?>" /></p>
 <P>Phone: <input type="text" name="phone" value="<?php echo htmlspecialchars(@$values['phone']) ?>"/></p>
 <P>Married:  <input type="checkbox" id="married" name="married"></p>
 <p>Please select your sex:</p>
  <input type="radio" id="male" name="sex" value="male">
  <label for="male">Male</label><br>
  <input type="radio" id="female" name="sex" value="female">
  <label for="female">Female</label><br>  
  <input type="radio" id="divers" name="sex" value="divers" checked>
  <label for="divers">Divers</label><br>
 <p><input type="submit" value="Submit"></p>

</form>
</body>
</html>