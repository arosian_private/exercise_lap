<!DOCTYPE html>
<html>
<head>
    <title>Register form</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <script src="script.js"></script>
    <meta charset="UTF-8">
</head>
    <header id="header">
        <img src="Images/small-header.jpg">
        <span class="slogan">Willkommen</span>
    </header>
    <nav id="navigation" class="flexcenter direction-row">
        <a href="/ILA_KPT/index.php" class="nav flexcenter">Register</a>
        <a href="#" class="nav flexcenter">Login</a>
    </nav>

    <?php
    $admin_id = "root";
    $admin_password = "root";
    session_start();
    $sessId = session_id();
    $cookie_name_id = "id";
    $cookie_name_name = "name";
    ?>

    

    <div style="width: 100%;" class="main flexcenter">
        <form class="form" method="post" class="flexcenter" action="login.php">
            <article>
                <label >Email</label>
                <input class="mandatory" placeholder="Email bitte schreiben" oninput="this.className = 'mandatory'" name="email">
            </article>
            <article>
                <label >Password <span style="color: red;">*</span></label>
                <input class="mandatory" placeholder="Passwort bitte schreiben" oninput="this.className = 'mandatory'" name="password">
            </article>
            <article>
            <?php                
                    if(!isset($_COOKIE[$cookie_name_id])) {
                        echo "Cookie named '" . $cookie_name_id . "' is not set!<br>";
                    } else {
                        echo "Cookie '" . $cookie_name_id . "' is set!<br>";                     
                            if($_COOKIE[$cookie_name_id] == session_id()){
                                echo "Session id is valid<br>";
                                if(isset($_COOKIE[$cookie_name_name])){
                                    echo "Welcome back: " . $_COOKIE[$cookie_name_name];
                                }                               
                            }
                            else{
                                echo "Session id is not valid<br>";
                            }
                        }                       
                          
                    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                        if($_POST["email"] == $admin_id && $_POST["password"] == $admin_password){
                            setcookie($cookie_name_id, session_id(), time() + (86400 * 30), "/");  
                            setcookie($cookie_name_name, $_POST["email"], time() + (86400 * 30), "/");  
                        }
                    }
                            
            ?>
            </article>
            <div>
                <input style="width: 100px;" type="submit" id="nextBtn" name="login" value="Login">
            </div>           
        </form>
    </div>
    <footer id="footer" class="flexcenter">
        <div class="custom-text">Hallo mein name ist Adrian Voicescu</div>
        <div class="custom-text">Sie koennen mich ueber diese E-Mail Adresse kontaktieren: </div>
        <div class="custom-text">voicescu.a@yahoo.com</div>
    </footer>
</body>
</html>