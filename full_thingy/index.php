﻿<!DOCTYPE html>
<html>
<head>
    <title>Register form</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <script src="script.js"></script>
    <meta charset="UTF-8">
</head>
<body>

    <header id="header">
        <img src="Images/small-header.jpg">
        <span class="slogan">Willkommen</span>
    </header>
    <nav id="navigation" class="flexcenter direction-row">
        <a href="#" class="nav flexcenter">Register</a>
        <a href="/ILA_KPT/login.php" class="nav flexcenter">Login</a>
    </nav>
    <div style="width: 100%;" class="main flexcenter direction-row flex-start">
        <form id="regForm" method="post" class="flexcenter" enctype="multipart/form-data" action="register.php">
        <!-- One "tab" for each step in the form: -->
        <section class="tab flexcenter">     
            <div class="sub-tab flexcenter">Konto</div>
            <article>
                <span style="color: red;">*</span>  - Felder sind obligatorisch
            </article>
            <p>Anrede: </p>
            <article>
                <input style="width: auto;" type="radio" id="male" name="gender" value="male">
                <label for="male">Male</label><br>
            </article>
            <article>
                <input style="width: auto;" type="radio" id="female" name="gender" value="female">
                <label for="female">Female</label><br>
            </article>
            <article>
                <input checked style="width: auto;" type="radio" id="other" name="gender" value="other">
                <label >Other</label>
            </article>
            <article>
                <label >Vorname <span style="color: red;">*</span></label>
                <input autofocus class="mandatory" placeholder="Geben Sie ihren Vorname ein" oninput="this.className = 'mandatory'" name="firstname">
            </article>
            <article>
                <label >Nachname <span style="color: red;">*</span></label>
                <input class="mandatory" placeholder="Geben Sie ihren Nachname ein" oninput="this.className = 'mandatory'" name="lastname">
            </article>
            <article>
                <label >Geburtstag</label>
                <input placeholder="TT.MM.JJJJ" oninput="this.className = ''" name="birthdate">
            </article>
            <article>
                <label >E-mail <span style="color: red;">*</span></label></label> 
                <input class="mandatory" placeholder="beispiel@gmail.com" oninput="this.className = 'mandatory'" name="email">
            </article>
            <article>
                <label >Passwort <span style="color: red;">*</span></label>
                <input type="password" class="password" placeholder="mind. eine Zahl, ein Groß-/Kleinbuchstabe, 8 Zeichnen" oninput="this.className = 'password'" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
                       title="Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more characters" name="password">
            </article>
           
            <article>
                <label >Passwort erneut schreiben <span style="color: red;">*</span></label>
                <input type="password" class="password" placeholder="Passwort erneut schreiben.." oninput="this.className = 'password'" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
                       title="Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more characters" name="password2">
            </article>
        </section>

        <section class="tab flexcenter">
            <div class="sub-tab flexcenter">Adresse</div>
            <article>
                <label >Strasse</label>
                <input placeholder="Ihre Adresse" oninput="this.className = ''" name="street">
            </article>
            <article>
                <label >Wohnort <span style="color: red;">*</span></label>
                <input class="mandatory" placeholder="Wohnort" oninput="this.className = 'mandatory'" name="city">
            </article>
            <article>
                <label >PLZ</label>
                <input type="number" placeholder="Postleitzahl" oninput="this.className = ''" name="postalcode">
            </article>
            <article>
                <label>Waehlen Sie ein Bundesland bitte:</label>
                <br />

                <select name="region">
                    <option value="Burgenland">Burgenland</option>
                    <option value="Kaernten">Kaernten</option>
                    <option value="Niederoesterreich">Niederoesterreich</option>
                    <option value="Oberoesterreich">Oberoesterreich</option>
                    <option value="Salzburg">Salzburg</option>
                    <option value="Steiermark">Steiermark</option>
                    <option value="Tirol">Tirol</option>
                    <option value="Vorarlberb">Vorarlberb</option>
                    <option value="Wien">Wien</option>
                </select>
            </article>
            <article>
                <label >Telefonnummer</label>
                <input placeholder="+xx xxxx xxxx" oninput="this.className = ''" name="phone">
            </article>
        </section>

        <section class="tab flexcenter">
            <div class="sub-tab flexcenter">Datei hochladen</div>         
            <article>
                <label >Datei hochladen:</label>
                <input type="file" name="fileToUpload"/>  
            </article>
        </section>

        <section class="tab buttons-container" style="overflow:auto;">
            <div style="float:right;">
                <button type="button" id="resetBtn" onclick="res()">Reset</button>
                <button type="button" id="nextBtn" onclick="save()" name="register"> Register </button>
            </div>
        </section>

        </form>

        <aside style="width: 30%;">
            <p style="margin-top: 40px; text-align: center;">
                Registrietungsform
            </p>
            <p>
                Diese Webseite hat ein Anmeldeformular. Um sich anzumelden, tragen Sie bitte die erforderlichen Informationen in die Felder ein.
            </p>
        </aside>
    </div>

    <footer id="footer" class="flexcenter">
        <div class="custom-text">Hallo mein name ist Adrian Voicescu</div>
        <div class="custom-text">Sie koennen mich ueber diese E-Mail Adresse kontaktieren: </div>
        <div class="custom-text">voicescu.a@yahoo.com</div>
    </footer>
</body>
</html>