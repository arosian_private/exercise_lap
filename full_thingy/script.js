function save() {
    // Exit the function if any field in the current tab is invalid:
    if (!validateForm()) return false;
    //...the form gets submitted:
    document.getElementById("regForm").submit();
}

function validateForm() {
    // This function deals with validation of the form fields
    var y, i, valid = true;

    y = document.getElementsByClassName("mandatory");
    passwords = document.getElementsByClassName("password");

    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].className += " invalid";
            // and set the current valid status to false:
            valid = false;
        }
    }
   
    // A loop that checks every password field in the current tab:
    for (i = 0; i < passwords.length; i++) {
        // If a field has bad input...
        if (!/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(passwords[i].value)) {
            // add an "invalid" class to the field:
            passwords[i].className += " invalid";
            // and set the current valid status to false:
            valid = false;
        }
    }

	if(passwords.length > 1){
		// Checking the second password if it is the same
		if (passwords[0].value !== passwords[1].value) {
			passwords[1].className += " invalid";
			valid = false
		}
	}

    return valid; // return the valid status
}

function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    x[n].className += " active";
}

function res() {
    document.getElementById("regForm").reset();
}